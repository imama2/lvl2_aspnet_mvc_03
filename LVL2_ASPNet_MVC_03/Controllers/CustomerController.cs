﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03.Models;
using System.Data.Entity;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customer_imamEntities Db = new db_customer_imamEntities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(Db.tbl_customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(Db.tbl_customer.Where(x => x.id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer customer)
        {
            using (var transactions = Db.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    if (ModelState.IsValid)
                    {
                        Db.tbl_customer.Add(customer);
                        Db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
                catch(Exception errorMessage)
                {
                    //-- tambahkan dengan notif --//
                    //-- error log history db --//
                     
                    var ErrorPosition = "Create_Customer";
                    db_customer_imamEntities dbError = new db_customer_imamEntities();
                    tbl_log_history error = new tbl_log_history
                    {
                        ErrorMesssage = errorMessage.Message,
                        ErrorPosition = ErrorPosition,
                        ErrorDate = DateTime.Now,
                    };
                    dbError.tbl_log_history.Add(error);
                    dbError.SaveChanges();
                    transactions.Rollback();
                    
                    ViewBag.MsgError = errorMessage.Message;
                    
                    return View();
                }
            }
        }


        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Db.tbl_customer.Where(x => x.id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer customer)
        {
            using (var transactions = Db.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add update logic here
                    Db.Entry(customer).State = EntityState.Modified;
                    Db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception errorMessage)
                {
                    var ErrorPosition = "Create_Edit";
                    db_customer_imamEntities dbError = new db_customer_imamEntities();
                    tbl_log_history error = new tbl_log_history
                    {
                        ErrorMesssage = errorMessage.Message,
                        ErrorPosition = ErrorPosition,
                        ErrorDate = DateTime.Now,
                    };
                    dbError.tbl_log_history.Add(error);
                    dbError.SaveChanges();
                    transactions.Rollback();
                    return View();
                }
            }
                
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Db.tbl_customer.Where(x => x.id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer customer)
        {
            using (var transactions = Db.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add delete logic here
                    customer = Db.tbl_customer.Where(x => x.id == id).FirstOrDefault();
                    Db.tbl_customer.Remove(customer);
                    Db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch(Exception errorMessage)
                {
                    var ErrorPosition = "Create_Edit";
                    db_customer_imamEntities dbError = new db_customer_imamEntities();
                    tbl_log_history error = new tbl_log_history
                    {
                        ErrorMesssage = errorMessage.Message,
                        ErrorPosition = ErrorPosition,
                        ErrorDate = DateTime.Now,
                    };
                    dbError.tbl_log_history.Add(error);
                    dbError.SaveChanges();
                    transactions.Rollback();
                    return View();
                }
            }
        }
    }
}
